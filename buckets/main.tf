
resource "google_storage_bucket" "kubernetes_tfstate_bucket" {
  name     = "kubernetes_tfstate_bucket"
  location = "EU"
  versioning = {enabled = true}
  project = "${var.kubernetes_project}"
   
}
resource "google_storage_bucket" "mongod_tfstate_bucket" {
  name     = "mongod_tfstate_bucket"
  location = "EU"
  versioning = {enabled = true}
  project = "${var.kubernetes_project}"
   
}
