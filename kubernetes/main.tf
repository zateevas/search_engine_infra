provider "google" {
  project     = "${var.project_name}"
  region      = "${var.location}"
}

terraform {
  backend "gcs" {
    bucket  = "kubernetes_tfstate_bucket"
    prefix  = "terraform/state"
  }
}
