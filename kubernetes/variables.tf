
variable "key_path" {
  
}

variable "project_name" {
  
}

variable "cluster_name" {
  default = "test"
}

variable "location" {
  default = "europe-west1-c"
}

variable "node_count" {
  default = 2
}

variable "machine_type" {
  default = "g1-small"
}


